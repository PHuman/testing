package test.by.training.humeniuk.dao.db;

import org.junit.Assert;
import org.junit.Test;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static by.training.humeniuk.dao.db.DBPropertyManager.getProperty;


public class ConnectionPoolTest {
    @Test
    public void testGetConnection() throws SQLException {
        final String URL = getProperty("url");
        final String USER = getProperty("user");
        final String PASSWORD = getProperty("password");
        DriverManager.registerDriver(new com.mysql.jdbc.Driver());
        Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);
        Assert.assertNotNull("Cant get connection", connection);
    }
}
