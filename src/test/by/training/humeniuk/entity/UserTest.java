package test.by.training.humeniuk.entity;

import by.training.humeniuk.entity.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class UserTest {
    private User user;

    @Before
    public void setUp() {
        user = new User();
    }

    @Test
    public void testGetLogin() {
        String result = user.getLogin();
        Assert.assertEquals(null, result);
    }
}
