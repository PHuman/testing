package test.by.training.humeniuk.util;

import by.training.humeniuk.entity.User;
import by.training.humeniuk.entity.UserType;
import org.junit.Assert;
import org.junit.Test;

public class ValidatorTest {
    private static final String LOGIN = "[а-яА-ЯёЁa-zA-Z]{4,10}";
    private static final String NAME = "[а-яА-ЯёЁa-zA-Z]{1,20}$";
    private static final String PASSWORD = ".{5,20}";
    @Test
    public void testValidateUser() {
        User user = new User("Pavel", "12345qwerty","Павел", "Гуменюк", UserType.STUDENT);
       boolean result = user.getLogin().matches(LOGIN) && user.getFirstName().matches(NAME) &&
                user.getLastName().matches(NAME) && user.getPassword().matches(PASSWORD);
        Assert.assertEquals(true, result);
    }
}
