package by.training.humeniuk.entity;

public enum UserType {
    STUDENT, TUTOR
}
