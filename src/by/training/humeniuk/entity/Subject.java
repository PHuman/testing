package by.training.humeniuk.entity;

public class Subject extends Entity {
    private String subjectName;

    public Subject() {
    }

    public Subject(int id, String subjectName) {
        super(id);
        this.subjectName = subjectName;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

}
