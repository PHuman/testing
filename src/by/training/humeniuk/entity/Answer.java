package by.training.humeniuk.entity;

public class Answer extends Entity {
    private String answerText;
    private int result;
    private int questionId;

    public Answer() {
    }

    public Answer(int id, String answerText, int result, int questionId) {
        super(id);
        this.answerText = answerText;
        this.result = result;
        this.questionId = questionId;
    }

    public Answer(String answerText, int questionId) {
        this.answerText = answerText;
        this.questionId = questionId;
    }

    public String getAnswerText() {
        return answerText;
    }

    public void setAnswerText(String answerText) {
        this.answerText = answerText;
    }

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Answer answer = (Answer) o;
        return result == answer.result && questionId == answer.questionId &&
                !(answerText != null ? !answerText.equals(answer.answerText) : answer.answerText != null);
    }

    @Override
    public int hashCode() {
        int result1 = answerText != null ? answerText.hashCode() : 0;
        result1 = 31 * result1 + result;
        result1 = 31 * result1 + questionId;
        return result1;
    }
}
