package by.training.humeniuk.entity;

public class Result extends Entity {
    private String login;
    private String subject;
    private String quiz;
    private int result;

    public Result() {
    }

    public Result(int id, String login, String subject, String quiz, int result) {
        super(id);
        this.login = login;
        this.subject = subject;
        this.quiz = quiz;
        this.result = result;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getQuiz() {
        return quiz;
    }

    public void setQuiz(String quiz) {
        this.quiz = quiz;
    }

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

}
