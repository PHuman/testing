package by.training.humeniuk.entity;


import java.util.ArrayList;

public class Question extends Entity {
    private String questionText;
    private ArrayList<Answer> answers;
    private int quizId;

    public Question() {
    }

    public Question(int id, String questionText) {
        super(id);
        this.questionText = questionText;
    }

    public Question(String questionText, int quizId) {
        this.questionText = questionText;
        this.quizId = quizId;
    }

    public int getQuizId() {
        return quizId;
    }

    public void setQuizId(int quizId) {
        this.quizId = quizId;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public ArrayList<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(ArrayList<Answer> answers) {
        this.answers = answers;
    }
}
