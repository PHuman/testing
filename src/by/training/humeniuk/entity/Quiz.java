package by.training.humeniuk.entity;

public class Quiz extends Entity {
    private String quizName;
    private int subjectId;

    public Quiz() {
    }

    public Quiz(int id, String quizName) {
        super(id);
        this.quizName = quizName;
    }

    public Quiz(String quizName, int subjectId) {
        this.quizName = quizName;
        this.subjectId = subjectId;
    }

    public int getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(int subjectId) {
        this.subjectId = subjectId;
    }

    public String getQuizName() {
        return quizName;
    }

    public void setQuizName(String quizName) {
        this.quizName = quizName;
    }

}
