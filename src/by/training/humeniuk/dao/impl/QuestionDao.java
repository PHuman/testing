package by.training.humeniuk.dao.impl;

import by.training.humeniuk.dao.Dao;
import by.training.humeniuk.dao.db.ConnectionPool;
import by.training.humeniuk.dao.exception.DaoException;
import by.training.humeniuk.entity.Question;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class QuestionDao implements Dao {
    private static QuestionDao instance = new QuestionDao();

    private static final String FIND_QUESTIONS_BY_QUIZ = "SELECT questionID , questionText FROM question WHERE quizID = ?";
    private static final String FIND_QUESTIONS_BY_TEXT = "SELECT questionID , questionText FROM question WHERE questionText = ?";
    private static final String INSERT_QUESTIONS = "INSERT INTO question (questionText, quizID) VALUES (?, ?)";

    private QuestionDao() {
    }

    public static QuestionDao getInstance() {
        return instance;
    }

    public void saveQuestion(Question question) throws DaoException {
        String questionText = question.getQuestionText();
        int quizID = question.getQuizId();
        try (Connection conn = ConnectionPool.getInstance().getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(INSERT_QUESTIONS)) {
            preparedStatement.setString(1, questionText);
            preparedStatement.setInt(2, quizID);
            preparedStatement.execute();
        } catch (SQLException e) {
            throw new DaoException("DaoException while QuestionDao.saveQuestion()", e);
        }
    }

    public Question findQuestionByText(String questionText) throws DaoException {
        Question question = null;
        try (Connection conn = ConnectionPool.getInstance().getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(FIND_QUESTIONS_BY_TEXT)) {
            preparedStatement.setString(1, questionText);
            ResultSet result = preparedStatement.executeQuery();
            while (result.next()) {
                question = new Question(result.getInt("questionID"), result.getString("questionText"));
            }
        } catch (SQLException e) {
            throw new DaoException("DaoException while QuestionDao.findQuestionByText()", e);
        }
        return question;
    }

    public ArrayList<Question> findQuestionByQuiz(int quizId) throws DaoException {
        ArrayList<Question> questions = new ArrayList<>();
        try (Connection conn = ConnectionPool.getInstance().getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(FIND_QUESTIONS_BY_QUIZ)) {
            preparedStatement.setInt(1, quizId);
            ResultSet result = preparedStatement.executeQuery();
            while (result.next()) {
                Question question = new Question(result.getInt("questionID"), result.getString("questionText"));
                question.setAnswers(AnswerDao.getInstance().findAnswerByQuestionId(question.getId()));
                questions.add(question);
            }
        } catch (SQLException e) {
            throw new DaoException("DaoException while QuestionDao.findQuestionByQuiz()", e);
        }
        return questions;
    }
}
