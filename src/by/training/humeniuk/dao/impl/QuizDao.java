package by.training.humeniuk.dao.impl;

import by.training.humeniuk.dao.Dao;
import by.training.humeniuk.dao.db.ConnectionPool;
import by.training.humeniuk.dao.exception.DaoException;
import by.training.humeniuk.entity.Quiz;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class QuizDao implements Dao {
    private static QuizDao instance = new QuizDao();

    private static final String FIND_QUIZ_BY_SUBJECT = "SELECT quizID,quizName FROM quiz WHERE subjectID = ?";
    private static final String FIND_QUIZ_ID_BY_NAME = "SELECT quizID FROM quiz WHERE quizName = ?";
    private static final String FIND_QUIZ_NAME_BY_ID = "SELECT quizName FROM quiz WHERE quizID = ?";
    private static final String INSERT_QUIZ = "INSERT INTO quiz (quizName, subjectID) VALUES (?, ?)";
    private static final String DELETE_QUIZ = "DELETE FROM quiz WHERE quizID = ?";

    private QuizDao() {
    }

    public static QuizDao getInstance() {
        return instance;
    }

    public ArrayList<Quiz> findQuizBySubject(int subjectId) throws DaoException {
        ArrayList<Quiz> quizs = new ArrayList<>();
        try (Connection conn = ConnectionPool.getInstance().getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(FIND_QUIZ_BY_SUBJECT)) {
            preparedStatement.setInt(1, subjectId);
            ResultSet result = preparedStatement.executeQuery();
            while (result.next()) {
                quizs.add(new Quiz(result.getInt("quizID"), result.getString("quizName")));
            }
        } catch (SQLException e) {
            throw new DaoException("DaoException while QuizDao.findQuizBySubject()", e);
        }
        return quizs;
    }

    public int findQuizIdByName(String quizName) throws DaoException {
        int quizId = 0;
        try (Connection conn = ConnectionPool.getInstance().getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(FIND_QUIZ_ID_BY_NAME)) {
            preparedStatement.setString(1, quizName);
            ResultSet result = preparedStatement.executeQuery();
            while (result.next()) {
                quizId = result.getInt("quizID");
            }
        } catch (SQLException e) {
            throw new DaoException("DaoException while QuizDao.findQuizIdByName()", e);
        }
        return quizId;
    }

    public String findQuizNameById(int quizId) throws DaoException {
        String quizName = null;
        try (Connection conn = ConnectionPool.getInstance().getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(FIND_QUIZ_NAME_BY_ID)) {
            preparedStatement.setInt(1, quizId);
            ResultSet result = preparedStatement.executeQuery();
            while (result.next()) {
                quizName = result.getString("quizName");
            }
        } catch (SQLException e) {
            throw new DaoException("DaoException while QuizDao.findQuizNameById()", e);
        }
        return quizName;
    }

    public void saveQuiz(Quiz quiz) throws DaoException {
        try (Connection conn = ConnectionPool.getInstance().getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(INSERT_QUIZ)) {
            preparedStatement.setString(1, quiz.getQuizName());
            preparedStatement.setInt(2, quiz.getSubjectId());
            preparedStatement.execute();
        } catch (SQLException e) {
            throw new DaoException("DaoException while QuizDao.saveQuiz()", e);
        }
    }

    public void deleteQuiz(int quizId) throws DaoException {
        try (Connection conn = ConnectionPool.getInstance().getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(DELETE_QUIZ)) {
            preparedStatement.setInt(1, quizId);
            preparedStatement.execute();
        } catch (SQLException e) {
            throw new DaoException("DaoException while QuizDao.deleteQuiz()", e);
        }
    }
}
