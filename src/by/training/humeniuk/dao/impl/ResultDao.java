package by.training.humeniuk.dao.impl;

import by.training.humeniuk.dao.Dao;
import by.training.humeniuk.dao.db.ConnectionPool;
import by.training.humeniuk.dao.exception.DaoException;
import by.training.humeniuk.entity.Quiz;
import by.training.humeniuk.entity.Result;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ResultDao implements Dao {
    private static ResultDao instance = new ResultDao();

    private static final String INSERT_RESULT = "INSERT INTO result (login, subjectID, quizID, result) VALUES (?, ?, ?, ?)";
    private static final String FIND_RESULT_BY_LOGIN = "SELECT resultID, login, subjectID, quizID, result FROM result WHERE login = ?";
    private static final String FIND_RESULT_BY_SUBJECT = "SELECT resultID, login, subjectID, quizID, result FROM result WHERE subjectID = ?";

    private ResultDao() {
    }

    public static ResultDao getInstance() {
        return instance;
    }

    public void saveResult(String login, int subjectId, int quizId, int result) throws DaoException {
        try (Connection conn = ConnectionPool.getInstance().getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(INSERT_RESULT)) {
            preparedStatement.setString(1, login);
            preparedStatement.setInt(2, subjectId);
            preparedStatement.setInt(3, quizId);
            preparedStatement.setInt(4, result);
            preparedStatement.execute();
        } catch (SQLException e) {
            throw new DaoException("DaoException while ResultDao.saveResult()", e);
        }
    }

    public ArrayList<Result> findResultByLogin(String login) throws DaoException {
        ArrayList<Result> results = new ArrayList<>();
        try (Connection conn = ConnectionPool.getInstance().getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(FIND_RESULT_BY_LOGIN)) {
            preparedStatement.setString(1, login);
            ResultSet result = preparedStatement.executeQuery();
            while (result.next()) {
                String subjectName = SubjectDao.getInstance().findSubjectNameById(result.getInt("subjectID"));
                String quizName = QuizDao.getInstance().findQuizNameById(result.getInt("quizID")) ;
                results.add(new Result(result.getInt("resultID"), result.getString("login"), subjectName, quizName,result.getInt("result")));
            }
        } catch (SQLException e) {
            throw new DaoException("DaoException while ResultDao.findResultByLogin()", e);
        }
        return results;
    }

    public ArrayList<Result> findResultBySubject(int subjectId) throws DaoException {
        ArrayList<Result> results = new ArrayList<>();
        try (Connection conn = ConnectionPool.getInstance().getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(FIND_RESULT_BY_SUBJECT)) {
            preparedStatement.setInt(1, subjectId);
            ResultSet result = preparedStatement.executeQuery();
            while (result.next()) {
                String subjectName = SubjectDao.getInstance().findSubjectNameById(result.getInt("subjectID"));
                String quizName = QuizDao.getInstance().findQuizNameById(result.getInt("quizID"));
                results.add(new Result(result.getInt("resultID"), result.getString("login"), subjectName, quizName,result.getInt("result")));
            }
        } catch (SQLException e) {
            throw new DaoException("DaoException while ResultDao.findResultBySubject()", e);
        }
        return results;
    }

}
