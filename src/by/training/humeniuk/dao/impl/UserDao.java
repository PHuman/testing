package by.training.humeniuk.dao.impl;

import by.training.humeniuk.dao.Dao;
import by.training.humeniuk.dao.db.ConnectionPool;
import by.training.humeniuk.dao.exception.DaoException;
import by.training.humeniuk.entity.User;
import by.training.humeniuk.entity.UserType;
import by.training.humeniuk.util.MD5;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDao implements Dao {
    private static UserDao instance = new UserDao();

    private static final String PUT_USER_TO_DB = "INSERT INTO users (login, password, firstName, lastName, type) VALUES (?, ?, ?, ?, ?)";
    private static final String FIND_USER_BY_LOGIN = "SELECT userID, login, password, firstName, lastName, type FROM users WHERE login = ?";

    private UserDao() {
    }

    public static UserDao getInstance() {
        return instance;
    }

    public void saveUser(User user) throws DaoException {
        String login = user.getLogin();
        String password = user.getPassword();
        String firstName = user.getFirstName();
        String lastName = user.getLastName();
        String type = user.getType().toString().toLowerCase();
        try (Connection conn = ConnectionPool.getInstance().getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(PUT_USER_TO_DB)) {
            preparedStatement.setString(1, login);
            preparedStatement.setString(2, MD5.md5(password));
            preparedStatement.setString(3, firstName);
            preparedStatement.setString(4, lastName);
            preparedStatement.setString(5, type);
            preparedStatement.execute();
        } catch (SQLException e) {
            throw new DaoException("DaoException while UserDao.saveUser()", e);
        }
    }

    public User findUserByLogin(String login) throws DaoException {
        User user = null;
        try (Connection conn = ConnectionPool.getInstance().getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(FIND_USER_BY_LOGIN)) {
            preparedStatement.setString(1, login);
            ResultSet result = preparedStatement.executeQuery();
            while (result.next()) {
                user = new User(result.getInt("userID"), result.getString("login"), result.getString("password"),
                        result.getString("firstName"), result.getString("lastName"),
                        UserType.valueOf(result.getString("type").toUpperCase()));
            }
        } catch (SQLException e) {
            throw new DaoException("DaoException while finding user in database", e);
        }
        return user;
    }

}
