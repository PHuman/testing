package by.training.humeniuk.dao.impl;

import by.training.humeniuk.dao.Dao;
import by.training.humeniuk.dao.db.ConnectionPool;
import by.training.humeniuk.dao.exception.DaoException;
import by.training.humeniuk.entity.Subject;

import java.sql.*;
import java.util.ArrayList;

public class SubjectDao implements Dao {
    private static SubjectDao instance = new SubjectDao();

    private static final String SELECT_ALL = "SELECT * FROM subject";
    private static final String FIND_SUBJECT_NAME_BY_ID = "SELECT subjectName FROM subject WHERE subjectID = ?";

    private SubjectDao() {
    }

    public static SubjectDao getInstance() {
        return instance;
    }

    public ArrayList<Subject> SelectAllSubjects() throws DaoException {
        ArrayList<Subject> subjects = new ArrayList<>();
        try (Connection conn = ConnectionPool.getInstance().getConnection();
             Statement statement = conn.createStatement()) {
            ResultSet result = statement.executeQuery(SELECT_ALL);
            while (result.next()) {
                subjects.add(new Subject(result.getInt("subjectID"), result.getString("subjectName")));
            }
        } catch (SQLException e) {
            throw new DaoException("DaoException while SubjectDao.SelectAllSubjects()", e);
        }
        return subjects;
    }

    public String findSubjectNameById(int subjectId) throws DaoException {
        String subjectName = null;
        try (Connection conn = ConnectionPool.getInstance().getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(FIND_SUBJECT_NAME_BY_ID)) {
            preparedStatement.setInt(1, subjectId);
            ResultSet result = preparedStatement.executeQuery();
            while (result.next()) {
                subjectName = result.getString("subjectName");
            }
        } catch (SQLException e) {
            throw new DaoException("DaoException while QuizDao.findSubjectNameById()", e);
        }
        return subjectName;
    }
}
