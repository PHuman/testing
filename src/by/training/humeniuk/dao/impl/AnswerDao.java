package by.training.humeniuk.dao.impl;

import by.training.humeniuk.dao.Dao;
import by.training.humeniuk.dao.db.ConnectionPool;
import by.training.humeniuk.dao.exception.DaoException;
import by.training.humeniuk.entity.Answer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class AnswerDao implements Dao {
    private static AnswerDao instance = new AnswerDao();

    private static final String FIND_ANSWERS_BY_QUESTION = "SELECT answerID, answerText, answer, questionID answer FROM answer WHERE questionID = ?";
    private static final String FIND_ANSWERS_BY_ID = "SELECT answerID, answerText, answer, questionID answer FROM answer WHERE answerID = ?";
    private static final String INSERT_ANSWER = "INSERT INTO answer (answerText, questionID) VALUES (?, ?)";
    private static final String SET_ANSWER = "UPDATE answer SET answer = 1 WHERE answerID = ?";

    private AnswerDao() {
    }

    public static AnswerDao getInstance() {
        return instance;
    }

    public void saveAnswer(Answer answer) throws DaoException {
        try (Connection conn = ConnectionPool.getInstance().getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(INSERT_ANSWER)) {
            preparedStatement.setString(1, answer.getAnswerText());
            preparedStatement.setInt(2, answer.getQuestionId());
            preparedStatement.execute();
        } catch (SQLException e) {
            throw new DaoException("DaoException while AnswerDao.saveAnswer()", e);
        }
    }

    public void setAnswer(int answerId) throws DaoException {
        try (Connection conn = ConnectionPool.getInstance().getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(SET_ANSWER)) {
            preparedStatement.setInt(1, answerId);
            preparedStatement.execute();
        } catch (SQLException e) {
            throw new DaoException("DaoException while AnswerDao.setAnswer()", e);
        }
    }

    public ArrayList<Answer> findAnswerByQuestionId(int questionId) throws DaoException {
        return getAnswers(questionId, FIND_ANSWERS_BY_QUESTION);
    }

    public Answer findAnswerById(int answerId) throws DaoException {
        return getAnswers(answerId, FIND_ANSWERS_BY_ID).get(0);
    }

    private ArrayList<Answer> getAnswers(int id, String sql) throws DaoException {
        ArrayList<Answer> answers = new ArrayList<>();
        try (Connection conn = ConnectionPool.getInstance().getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(sql)) {
            preparedStatement.setInt(1, id);
            ResultSet result = preparedStatement.executeQuery();
            while (result.next()) {
                answers.add(new Answer(result.getInt(1), result.getString(2), result.getInt(3), result.getInt(4)));
            }
        } catch (SQLException e) {
            throw new DaoException("DaoException while AnswerDao.getAnswers()", e);
        }
        return answers;
    }
}
