package by.training.humeniuk.dao.db;

import by.training.humeniuk.dao.exception.DaoException;
import org.apache.log4j.Logger;

import javax.annotation.PreDestroy;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.LinkedBlockingQueue;

import static by.training.humeniuk.dao.db.DBPropertyManager.getProperty;


public class ConnectionPool {
    private static final Logger LOG = Logger.getLogger(ConnectionPool.class);
    private LinkedBlockingQueue<ProxyConnection> connectionQueue;
    private final static String URL = getProperty("url");
    private final static String USER = getProperty("user");
    private final static String PASSWORD = getProperty("password");
    private final static int POOL_SIZE = Integer.parseInt(getProperty("poolSize"));

    private ConnectionPool(int poolSize) {
        connectionQueue = new LinkedBlockingQueue<>(poolSize);
        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
        } catch (SQLException e) {
            LOG.fatal("ConnectionPool(int poolSize)", e);
            throw new RuntimeException(e);
        }
        for (int i = 0; i < poolSize; i++) {
            connectionQueue.offer(createConnection());
        }
        if (connectionQueue.size() < poolSize * 0.75) {
            LOG.fatal("ConnectionPool don`t created");
            throw new RuntimeException();
        }
    }

    /**
     * @return new ProxyConnection
     */
    private ProxyConnection createConnection() {
        ProxyConnection pc = null;
        try {
            Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);
            pc = new ProxyConnection(connection);
        } catch (SQLException e) {
            LOG.error("ConnectionPool.createConnection()", e);
        }
        return pc;
    }

    /**
     * @return lazy instance of Connection pool
     */
    public static ConnectionPool getInstance() {
        return SingletonHolder.INSTANCE;
    }

    private static class SingletonHolder {
        private static final ConnectionPool INSTANCE = new ConnectionPool(POOL_SIZE);
    }

    /**
     * @return ProxyConnection from pool
     */
    public ProxyConnection getConnection() throws DaoException {
        ProxyConnection connection;
        try {
            connection = connectionQueue.take();
        } catch (InterruptedException e) {
            throw new DaoException("ConnectionPool.getConnection()",e);
        }
        return connection;
    }

    /**
     * @param connection return connection to pool
     */
    public void closeConnection(ProxyConnection connection) {
        try {
            connectionQueue.put(connection);
        } catch (InterruptedException e) {
            LOG.error(e);
            connectionQueue.offer(createConnection());
        }
    }

    /**
     * close all connections in pool
     */
    @PreDestroy
    public void closeAll() {
        connectionQueue.forEach(ProxyConnection::doClose);
    }

}


