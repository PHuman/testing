package by.training.humeniuk.dao.db;

import java.util.ResourceBundle;

public class DBPropertyManager {
    private final static ResourceBundle resourceBundle = ResourceBundle.getBundle("resources.config");

    private DBPropertyManager() {
    }
    
    public static String getProperty(String key) {
        return resourceBundle.getString(key);
    }
}
