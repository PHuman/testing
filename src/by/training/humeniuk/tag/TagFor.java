package by.training.humeniuk.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

public class TagFor extends TagSupport {
    private int from;
    private int to;

    public void setTo(int to) {
        this.to = to;
    }

    @Override
    public int doStartTag() throws JspException {
        from = 0;
        if (to <= 0) {
            return SKIP_BODY;
        }
        from++;
        return EVAL_BODY_INCLUDE;
    }

    @Override
    public int doAfterBody() throws JspException {
        while (from < to) {
            from++;
            return EVAL_BODY_AGAIN;
        }
        return SKIP_BODY;
    }
}
