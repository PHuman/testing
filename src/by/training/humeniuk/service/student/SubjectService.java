package by.training.humeniuk.service.student;

import by.training.humeniuk.dao.exception.DaoException;
import by.training.humeniuk.dao.impl.QuizDao;
import by.training.humeniuk.entity.Quiz;
import by.training.humeniuk.service.exception.ServiceException;

import java.util.ArrayList;

public abstract class SubjectService {

    public static ArrayList<Quiz> getQuizs(int subjectId) throws ServiceException {
        ArrayList<Quiz> quizs;
        try {
            quizs = QuizDao.getInstance().findQuizBySubject(subjectId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
        return quizs;
    }
}
