package by.training.humeniuk.service.student;

import by.training.humeniuk.dao.exception.DaoException;
import by.training.humeniuk.dao.impl.ResultDao;
import by.training.humeniuk.entity.Result;
import by.training.humeniuk.service.exception.ServiceException;

import java.util.ArrayList;
import java.util.Comparator;

public abstract class StudentMenuService {

    public static ArrayList<Result> getResults(String login) throws ServiceException {
        ArrayList<Result> results;
        try {
            results = ResultDao.getInstance().findResultByLogin(login);
            results.sort(Comparator.comparing(Result::getSubject).
                    thenComparing(Result::getQuiz).
                    thenComparing(Result::getResult));
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
        return results;
    }
}
