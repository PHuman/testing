package by.training.humeniuk.service.student;

import by.training.humeniuk.dao.exception.DaoException;
import by.training.humeniuk.dao.impl.QuestionDao;
import by.training.humeniuk.entity.Question;
import by.training.humeniuk.service.exception.ServiceException;

import java.util.ArrayList;

public abstract class QuizService {

    public static ArrayList<Question> getQuestions(int quizId) throws ServiceException {
        ArrayList<Question> questions;
        try {
            questions = QuestionDao.getInstance().findQuestionByQuiz(quizId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
        return questions;
    }
}
