package by.training.humeniuk.service.student;

import by.training.humeniuk.dao.exception.DaoException;
import by.training.humeniuk.dao.impl.AnswerDao;
import by.training.humeniuk.dao.impl.ResultDao;
import by.training.humeniuk.entity.Answer;
import by.training.humeniuk.service.exception.ServiceException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class QuestionService {

    public static void saveResult(int finalResult, String login, int subjectId, int quizId) throws ServiceException {
        try {
            ResultDao.getInstance().saveResult(login, subjectId, quizId, finalResult);
        } catch (DaoException e) {
            throw new ServiceException();
        }
    }

    public static int getFinalResult(String[] answers, String[] questions) throws ServiceException {
        final int[] result = {0};
        Map<Integer, List<Answer>> userAnswers = getUserAnswers(answers);
        Map<Integer, List<Answer>> trueAnswers = getTrueAnswers(questions);
        userAnswers.forEach((key, value) -> {
            if (trueAnswers.get(key).equals(value)) {
                result[0]++;
            }
        });
        return result[0] * 100 / trueAnswers.size();
    }

    private static Map<Integer, List<Answer>> getUserAnswers(String[] answers) throws ServiceException {
        final boolean[] flag = new boolean[1];
        Map<Integer, List<Answer>> userAnswers = Stream.of(answers).
                map(Integer::parseInt).
                map(id -> {
                    Answer answer = null;
                    try {
                        answer = AnswerDao.getInstance().findAnswerById(id);
                    } catch (DaoException e) {
                        flag[0] = true;
                    }
                    return answer;
                }).
                collect(Collectors.groupingBy(Answer::getQuestionId));
        if (flag[0]) {
            throw new ServiceException();
        }
        return userAnswers;
    }

    private static Map<Integer, List<Answer>> getTrueAnswers(String[] questions) throws ServiceException {
        final boolean[] flag = new boolean[1];
        Map<Integer, List<Answer>> trueAnswers = Stream.of(questions).
                map(Integer::parseInt).
                flatMap(id -> {
                    ArrayList<Answer> answers = null;
                    try {
                        answers = AnswerDao.getInstance().findAnswerByQuestionId(id);
                    } catch (DaoException e) {
                        flag[0] = true;
                    }
                    return answers != null ? answers.stream() : null;
                }).
                filter(answer -> answer.getResult() > 0).
                collect(Collectors.groupingBy(Answer::getQuestionId));
        if (flag[0]) {
            throw new ServiceException();
        }
        return trueAnswers;
    }
}

