package by.training.humeniuk.service.authorization;

import by.training.humeniuk.dao.exception.DaoException;
import by.training.humeniuk.dao.impl.SubjectDao;
import by.training.humeniuk.dao.impl.UserDao;
import by.training.humeniuk.entity.Subject;
import by.training.humeniuk.entity.User;
import by.training.humeniuk.service.exception.ServiceException;

import java.util.ArrayList;

public abstract class LoginService {
    public static User getUser(String login) throws ServiceException {
        User user;
        try {
            user = UserDao.getInstance().findUserByLogin(login);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
        return user;
    }

    public static ArrayList<Subject> getSubjects() throws ServiceException {
        ArrayList<Subject> subjects;
        try {
            subjects = SubjectDao.getInstance().SelectAllSubjects();
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
        return subjects;
    }
}
