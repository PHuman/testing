package by.training.humeniuk.service.authorization;

import by.training.humeniuk.dao.exception.DaoException;
import by.training.humeniuk.dao.impl.UserDao;
import by.training.humeniuk.entity.User;
import by.training.humeniuk.service.exception.ServiceException;

public abstract class RegistrationService {

    public static void saveUser(User user) throws ServiceException {
        try {
            UserDao.getInstance().saveUser(user);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }
}
