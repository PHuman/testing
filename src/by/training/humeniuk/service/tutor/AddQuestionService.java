package by.training.humeniuk.service.tutor;

import by.training.humeniuk.dao.exception.DaoException;
import by.training.humeniuk.dao.impl.AnswerDao;
import by.training.humeniuk.dao.impl.QuestionDao;
import by.training.humeniuk.dao.impl.QuizDao;
import by.training.humeniuk.entity.Answer;
import by.training.humeniuk.entity.Question;
import by.training.humeniuk.service.exception.ServiceException;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class AddQuestionService {

    public static int getQuizId(String quizName) throws ServiceException {
        int quizId;
        try {
            quizId = QuizDao.getInstance().findQuizIdByName(quizName);
        } catch (DaoException e) {
            throw new ServiceException();
        }
        return quizId;
    }

    public static List<Integer> getQuestionsId(int finalQuizId, String[] questions) throws ServiceException {
        final boolean[] flag = new boolean[1];
        List<Integer> questionsId = Stream.of(questions).
                map(s -> new Question(s, finalQuizId)).
                peek(question -> {
                    try {
                        QuestionDao.getInstance().saveQuestion(question);
                    } catch (DaoException e) {
                        flag[0] = true;
                    }
                }).
                map(question -> {
                    int id = 0;
                    try {
                        return QuestionDao.getInstance().findQuestionByText(question.getQuestionText()).getId();
                    } catch (DaoException e) {
                        flag[0] = true;
                    }
                    return id;
                }).
                collect(Collectors.toList());
        if (flag[0]) {
            throw new ServiceException();
        }
        return questionsId;
    }

    public static void saveAnswers(List<String> answersText, List<Integer> questionsId) throws ServiceException {
        ArrayList<Answer> answers = new ArrayList<>();
        int i = 0;
        for (int j = 0; j < answersText.size(); j++) {
            if (answersText.get(j) != null && !answersText.get(j).isEmpty()) {
                answers.add(new Answer(answersText.get(j), questionsId.get(i)));
            }
            if ((j + 1) % 4 == 0) {
                i++;
            }
        }
        for (Answer answer : answers) {
            try {
                AnswerDao.getInstance().saveAnswer(answer);
            } catch (DaoException e) {
                throw new ServiceException();
            }
        }
    }

}
