package by.training.humeniuk.service.tutor;

import by.training.humeniuk.dao.exception.DaoException;
import by.training.humeniuk.dao.impl.AnswerDao;
import by.training.humeniuk.service.exception.ServiceException;

import java.util.stream.Stream;

public abstract class SetAnswerService {

    public static void saveAnswers(String[] answers) throws ServiceException {
        final boolean[] flag = new boolean[1];
        Stream.of(answers).map(Integer::parseInt).
                forEach(id -> {
                    try {
                        AnswerDao.getInstance().setAnswer(id);
                    } catch (DaoException e) {
                        flag[0] = true;
                    }
                });
        if (flag[0]) {
            throw new ServiceException();
        }
    }
}
