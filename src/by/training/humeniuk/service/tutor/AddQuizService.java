package by.training.humeniuk.service.tutor;

import by.training.humeniuk.dao.exception.DaoException;
import by.training.humeniuk.dao.impl.QuizDao;
import by.training.humeniuk.entity.Quiz;
import by.training.humeniuk.service.exception.ServiceException;

public abstract class AddQuizService {

    public static void saveQuiz(int subjectId, String quizName) throws ServiceException {
        try {
            Quiz quiz = new Quiz(quizName, subjectId);
            QuizDao.getInstance().saveQuiz(quiz);
        } catch (DaoException e) {
            throw new ServiceException();
        }
    }
}
