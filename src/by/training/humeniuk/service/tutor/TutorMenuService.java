package by.training.humeniuk.service.tutor;

import by.training.humeniuk.dao.exception.DaoException;
import by.training.humeniuk.dao.impl.ResultDao;
import by.training.humeniuk.entity.Result;
import by.training.humeniuk.service.exception.ServiceException;

import java.util.ArrayList;
import java.util.Comparator;

public abstract class TutorMenuService {

    public static ArrayList<Result> getResults(int subjectId) throws ServiceException {
        ArrayList<Result> results;
        try {
            results = ResultDao.getInstance().findResultBySubject(subjectId);
            results.sort(Comparator.comparing(Result::getLogin).
                    thenComparing(Result::getQuiz).
                    thenComparing(Result::getResult));
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
        return results;
    }
}
