package by.training.humeniuk.service.tutor;

import by.training.humeniuk.dao.exception.DaoException;
import by.training.humeniuk.dao.impl.QuizDao;
import by.training.humeniuk.service.exception.ServiceException;

public abstract class DeleteQuizService {

    public static void deleteQuiz(int quizId) throws ServiceException {
        try {
            QuizDao.getInstance().deleteQuiz(quizId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }
}
