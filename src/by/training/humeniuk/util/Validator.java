package by.training.humeniuk.util;

import by.training.humeniuk.entity.User;

public abstract class Validator {
    private static final String LOGIN = "[а-яА-ЯёЁa-zA-Z]{4,10}";
    private static final String NAME = "[а-яА-ЯёЁa-zA-Z]{1,20}";
    private static final String PASSWORD = ".{5,20}";

    public static boolean validateUser(User user) {
        boolean valid = false;
        if (user.getLogin().matches(LOGIN) && user.getFirstName().matches(NAME) &&
                user.getLastName().matches(NAME) && user.getPassword().matches(PASSWORD)) {
            valid = true;
        }
        return valid;
    }
}
