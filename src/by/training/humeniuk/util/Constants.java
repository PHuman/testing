package by.training.humeniuk.util;

public abstract class Constants {
    /*   For student!*/
    public static final String QUESTION = "/jsp/student/questions.jsp";
    public static final String QUIZ = "/jsp/student/quiz.jsp";
    public static final String RESULT = "/jsp/student/result.jsp";
    public static final String SUBJECT = "/jsp/student/subject.jsp";
    public static final String STUDENT_MENU = "/jsp/student/studentmenu.jsp";
    public static final String STUDENT_RESULT = "/jsp/student/studentresult.jsp";
    /*   For tutor!*/
    public static final String ADD_QUESTION = "/jsp/tutor/addquestion.jsp";
    public static final String ADD_QUIZ = "/jsp/tutor/addquiz.jsp";
    public static final String SET_ANSWER = "/jsp/tutor/setanswer.jsp";
    public static final String TUTOR_MENU = "/jsp/tutor/tutormenu.jsp";
    public static final String DELETE_QUIZ = "/jsp/tutor/deletequiz.jsp";
    public static final String CURRENT_RESULT = "/jsp/tutor/currentresult.jsp";
    /*   For all!*/
    public static final String ERROR = "/jsp/error.jsp";
    public static final String LOGIN = "/jsp/login.jsp";
    public static final String REGISTRATION = "/jsp/registration.jsp";
}
