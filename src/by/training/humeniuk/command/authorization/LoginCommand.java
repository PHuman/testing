package by.training.humeniuk.command.authorization;

import by.training.humeniuk.command.Command;
import by.training.humeniuk.entity.Subject;
import by.training.humeniuk.entity.User;
import by.training.humeniuk.entity.UserType;
import by.training.humeniuk.service.authorization.LoginService;
import by.training.humeniuk.service.exception.ServiceException;
import by.training.humeniuk.util.Constants;
import by.training.humeniuk.util.MD5;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;

public class LoginCommand implements Command {
    private static LoginCommand instance = new LoginCommand();
    private static final Logger LOG = Logger.getLogger(LoginCommand.class);

    private LoginCommand() {
    }

    public static LoginCommand getInstance() {
        return instance;
    }

    @Override
    public String execute(HttpServletRequest request) {
        if ("registration".equals(request.getParameter("choice"))) {
            return Constants.REGISTRATION;
        }
        HttpSession session = request.getSession();
        String page;
        String login = request.getParameter("login");
        String password = MD5.md5(request.getParameter("password"));
        User user = null;
        try {
            user = LoginService.getUser(login);
        } catch (ServiceException e) {
            LOG.error(e);
        }
        if (user == null) {
            page = Constants.LOGIN;
            request.setAttribute("errorLoginPassMessage", true);
        } else if (password.equals(user.getPassword())) {
            session.setAttribute("userLogin", user.getLogin());
            session.setAttribute("userType", user.getType());
            session.setAttribute("userFirstName", user.getFirstName());
            session.setAttribute("userLastName", user.getLastName());
            if (user.getType().equals(UserType.STUDENT)) {
                page = Constants.STUDENT_MENU;
            } else {
                try {
                    request.setAttribute("subjects", LoginService.getSubjects());
                    page = Constants.TUTOR_MENU;
                } catch (ServiceException e) {
                    LOG.error(e);
                    page = Constants.ERROR;
                }
            }
        } else {
            page = Constants.LOGIN;
            request.setAttribute("errorLoginPassMessage", true);
        }
        return page;
    }

}
