package by.training.humeniuk.command.authorization;

import by.training.humeniuk.command.Command;
import by.training.humeniuk.entity.User;
import by.training.humeniuk.entity.UserType;
import by.training.humeniuk.service.authorization.RegistrationService;
import by.training.humeniuk.service.exception.ServiceException;
import by.training.humeniuk.util.Constants;
import by.training.humeniuk.util.Validator;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

public class RegistrationCommand implements Command {
    private static RegistrationCommand instance = new RegistrationCommand();
    private static final Logger LOG = Logger.getLogger(RegistrationCommand.class);
    private static final String SECRET_KEY = "tutor";

    private RegistrationCommand() {
    }

    public static RegistrationCommand getInstance() {
        return instance;
    }

    @Override
    public String execute(HttpServletRequest request) {
        String page = Constants.REGISTRATION;
        UserType type;
        String login = request.getParameter("login");
        String firstName = request.getParameter("first_name");
        String lastName = request.getParameter("last_name");
        String password = request.getParameter("password");
        String confPassword = request.getParameter("conf_password");
        String secretKey = request.getParameter("secret");
        if (secretKey == null || secretKey.isEmpty()) {
            type = UserType.STUDENT;
        } else if (SECRET_KEY.equals(secretKey)) {
            type = UserType.TUTOR;
        } else {
            request.setAttribute("secretError", true);
            return page;
        }
        if (login == null || firstName == null || lastName == null || password == null) {
            request.setAttribute("emptyFields", true);
            return page;
        }
        if (!password.equals(confPassword)) {
            request.setAttribute("passwordError", true);
            return page;
        }
        User user = new User(login, password, firstName, lastName, type);
        if (Validator.validateUser(user)) {
            try {
                RegistrationService.saveUser(user);
                request.setAttribute("successRegistration", true);
                page = Constants.LOGIN;
            } catch (ServiceException e) {
                LOG.error(e);
                page = Constants.ERROR;
            }
        } else {
            request.setAttribute("errorRegistration", true);
        }
        return page;
    }

}
