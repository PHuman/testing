package by.training.humeniuk.command.common;

import by.training.humeniuk.command.Command;

import javax.servlet.http.HttpServletRequest;

public class NullCommand implements Command {
    private static NullCommand instance = new NullCommand();

    private NullCommand() {
    }

    public static NullCommand getInstance() {
        return instance;
    }

    @Override
    public String execute(HttpServletRequest request) {
        return null;
    }
}
