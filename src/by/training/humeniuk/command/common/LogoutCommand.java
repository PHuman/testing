package by.training.humeniuk.command.common;

import by.training.humeniuk.command.Command;
import by.training.humeniuk.util.Constants;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class LogoutCommand implements Command {
    private static LogoutCommand instance = new LogoutCommand();

    private LogoutCommand() {
    }

    public static LogoutCommand getInstance() {
        return instance;
    }

    @Override
    public String execute(HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.invalidate();
        return Constants.LOGIN;
    }
}
