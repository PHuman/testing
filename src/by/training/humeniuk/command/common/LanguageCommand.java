package by.training.humeniuk.command.common;

import by.training.humeniuk.command.Command;
import by.training.humeniuk.util.Constants;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Locale;

public class LanguageCommand implements Command {
    private static LanguageCommand instance = new LanguageCommand();

    private LanguageCommand() {
    }

    public static LanguageCommand getInstance() {
        return instance;
    }

    @Override
    public String execute(HttpServletRequest request) {
        HttpSession session = request.getSession();
        String language = request.getParameter("language");
        Locale locale = new Locale(language);
        session.setAttribute("locale", locale);
        return Constants.LOGIN;
    }
}
