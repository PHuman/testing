package by.training.humeniuk.command.student;

import by.training.humeniuk.command.Command;
import by.training.humeniuk.service.exception.ServiceException;
import by.training.humeniuk.service.student.SubjectService;
import by.training.humeniuk.util.Constants;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class SubjectCommand implements Command {
    private static SubjectCommand instance = new SubjectCommand();
    private static final Logger LOG = Logger.getLogger(SubjectCommand.class);

    private SubjectCommand() {
    }

    public static SubjectCommand getInstance() {
        return instance;
    }

    @Override
    public String execute(HttpServletRequest request) {
        String page = Constants.QUIZ;
        int subjectId = Integer.parseInt(request.getParameter("subject"));
        HttpSession session = request.getSession();
        session.setAttribute("subjectId", subjectId);
        try {
            request.setAttribute("quizs", SubjectService.getQuizs(subjectId));
        } catch (ServiceException e) {
            LOG.error(e);
            page = Constants.ERROR;
        }
        return page;
    }
}
