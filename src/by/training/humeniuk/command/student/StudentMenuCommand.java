package by.training.humeniuk.command.student;

import by.training.humeniuk.command.Command;
import by.training.humeniuk.dao.exception.DaoException;
import by.training.humeniuk.dao.impl.ResultDao;
import by.training.humeniuk.entity.Result;
import by.training.humeniuk.service.authorization.LoginService;
import by.training.humeniuk.service.exception.ServiceException;
import by.training.humeniuk.service.student.StudentMenuService;
import by.training.humeniuk.util.Constants;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Comparator;

public class StudentMenuCommand implements Command {
    private static StudentMenuCommand instance = new StudentMenuCommand();
    private static final Logger LOG = Logger.getLogger(StudentMenuCommand.class);

    private StudentMenuCommand() {
    }

    public static StudentMenuCommand getInstance() {
        return instance;
    }

    @Override
    public String execute(HttpServletRequest request) {
        String page = Constants.ERROR;
        HttpSession session = request.getSession();
        String choice = request.getParameter("choice");
        if ("result".equals(choice)) {
            String login = session.getAttribute("userLogin").toString();
            ArrayList<Result> results = null;
            try {
                results = StudentMenuService.getResults(login);
            } catch (ServiceException e) {
                LOG.error(e);
            }
            if (results != null && !results.isEmpty()) {
                request.setAttribute("result", results);
                page = Constants.STUDENT_RESULT;
            } else {
                request.setAttribute("error", true);
                page = Constants.STUDENT_MENU;
            }
        } else if ("subject".equals(choice)) {
            try {
                request.setAttribute("subjects", LoginService.getSubjects());
                page = Constants.SUBJECT;
            } catch (ServiceException e) {
                LOG.error(e);
                page = Constants.ERROR;
            }
        }
        return page;
    }


}
