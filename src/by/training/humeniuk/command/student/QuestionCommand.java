package by.training.humeniuk.command.student;

import by.training.humeniuk.command.Command;
import by.training.humeniuk.service.exception.ServiceException;
import by.training.humeniuk.service.student.QuestionService;
import by.training.humeniuk.util.Constants;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class QuestionCommand implements Command {
    private static QuestionCommand instance = new QuestionCommand();
    private static final Logger LOG = Logger.getLogger(QuestionCommand.class);

    private QuestionCommand() {
    }

    public static QuestionCommand getInstance() {
        return instance;
    }

    @Override
    public String execute(HttpServletRequest request) {
        HttpSession session = request.getSession();
        String page = Constants.RESULT;
        String[] answers = request.getParameterValues("answer");
        String[] questions = request.getParameterValues("question");
        int finalResult;
        if (answers != null) {
            try {
                finalResult = QuestionService.getFinalResult(answers, questions);
            } catch (ServiceException e) {
                LOG.error(e);
                return Constants.ERROR;
            }
        } else {
            finalResult = 0;
        }
        request.setAttribute("result", finalResult);
        String login = session.getAttribute("userLogin").toString();
        int subjectId = Integer.parseInt(session.getAttribute("subjectId").toString());
        int quizId = Integer.parseInt(session.getAttribute("quizId").toString());
        try {
            QuestionService.saveResult(finalResult, login, subjectId, quizId);
        } catch (ServiceException e) {
            LOG.error(e);
            page = Constants.ERROR;
        }
        return page;
    }
}