package by.training.humeniuk.command.student;

import by.training.humeniuk.command.Command;
import by.training.humeniuk.util.Constants;

import javax.servlet.http.HttpServletRequest;

public class ResultCommand implements Command {
    private static ResultCommand instance = new ResultCommand();

    private ResultCommand() {
    }

    public static ResultCommand getInstance() {
        return instance;
    }

    @Override
    public String execute(HttpServletRequest request) {
        return Constants.STUDENT_MENU;
    }
}
