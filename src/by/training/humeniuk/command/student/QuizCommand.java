package by.training.humeniuk.command.student;

import by.training.humeniuk.command.Command;
import by.training.humeniuk.service.exception.ServiceException;
import by.training.humeniuk.service.student.QuizService;
import by.training.humeniuk.util.Constants;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class QuizCommand implements Command {
    private static QuizCommand instance = new QuizCommand();
    private static final Logger LOG = Logger.getLogger(QuizCommand.class);

    private QuizCommand() {
    }

    public static QuizCommand getInstance() {
        return instance;
    }

    @Override
    public String execute(HttpServletRequest request) {
        String page = Constants.QUESTION;
        String quiz = request.getParameter("quiz");
        int quizId = Integer.parseInt(quiz);
        HttpSession session = request.getSession();
        try {
            request.setAttribute("questions", QuizService.getQuestions(quizId));
            session.setAttribute("quizId", quizId);
        } catch (ServiceException e) {
            LOG.error(e);
            page = Constants.ERROR;
        }
        return page;
    }
}