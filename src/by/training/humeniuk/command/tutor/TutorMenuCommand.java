package by.training.humeniuk.command.tutor;

import by.training.humeniuk.command.Command;
import by.training.humeniuk.entity.Result;
import by.training.humeniuk.service.exception.ServiceException;
import by.training.humeniuk.service.student.SubjectService;
import by.training.humeniuk.service.tutor.TutorMenuService;
import by.training.humeniuk.util.Constants;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;

public class TutorMenuCommand implements Command {
    private static TutorMenuCommand instance = new TutorMenuCommand();
    private static final Logger LOG = Logger.getLogger(TutorMenuCommand.class);

    private TutorMenuCommand() {
    }

    public static TutorMenuCommand getInstance() {
        return instance;
    }

    @Override
    public String execute(HttpServletRequest request) {
        String page = Constants.ERROR;
        String choice = request.getParameter("choice");
        int subjectId = Integer.parseInt(request.getParameter("subject"));
        if ("result".equals(choice)) {
            ArrayList<Result> results = null;
            try {
                results = TutorMenuService.getResults(subjectId);
            } catch (ServiceException e) {
                LOG.error(e);
            }
            if (results != null && !results.isEmpty()) {
                request.setAttribute("result", results);
                page = Constants.CURRENT_RESULT;
            } else {
                request.setAttribute("error", true);
                page = Constants.TUTOR_MENU;
            }
        } else if ("delete".equals(choice)) {
            try {
                request.setAttribute("quizs", SubjectService.getQuizs(subjectId));
                page = Constants.DELETE_QUIZ;
            } catch (ServiceException e) {
                LOG.error(e);
            }
        } else if ("add".equals(choice)) {
            HttpSession session = request.getSession();
            session.setAttribute("subjectId", subjectId);
            page = Constants.ADD_QUIZ;
        }
        return page;
    }
}