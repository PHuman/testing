package by.training.humeniuk.command.tutor;

import by.training.humeniuk.command.Command;
import by.training.humeniuk.service.authorization.LoginService;
import by.training.humeniuk.service.exception.ServiceException;
import by.training.humeniuk.service.student.QuizService;
import by.training.humeniuk.service.tutor.SetAnswerService;
import by.training.humeniuk.util.Constants;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class SetAnswerCommand implements Command {
    private static SetAnswerCommand instance = new SetAnswerCommand();
    private static final Logger LOG = Logger.getLogger(SetAnswerCommand.class);

    private SetAnswerCommand() {
    }

    public static SetAnswerCommand getInstance() {
        return instance;
    }

    @Override
    public String execute(HttpServletRequest request) {
        String page = Constants.ERROR;
        String[] answers = request.getParameterValues("answer");
        if (answers != null) {
            try {
                SetAnswerService.saveAnswers(answers);
                request.setAttribute("subjects", LoginService.getSubjects());
                request.setAttribute("success", true);
                page = Constants.TUTOR_MENU;
            } catch (ServiceException e) {
                LOG.error(e);
            }
        } else {
            request.setAttribute("error", true);
            HttpSession session = request.getSession();
            int quizId = Integer.parseInt(session.getAttribute("quizId").toString());
            try {
                request.setAttribute("questions", QuizService.getQuestions(quizId));
                page = Constants.SET_ANSWER;
            } catch (ServiceException e) {
                LOG.error(e);
            }
        }
        return page;
    }
}
