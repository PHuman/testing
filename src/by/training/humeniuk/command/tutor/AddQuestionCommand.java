package by.training.humeniuk.command.tutor;

import by.training.humeniuk.command.Command;
import by.training.humeniuk.service.exception.ServiceException;
import by.training.humeniuk.service.student.QuizService;
import by.training.humeniuk.service.tutor.AddQuestionService;
import by.training.humeniuk.util.Constants;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Arrays;
import java.util.List;

public class AddQuestionCommand implements Command {
    private static AddQuestionCommand instance = new AddQuestionCommand();
    private static final Logger LOG = Logger.getLogger(AddQuestionCommand.class);

    private AddQuestionCommand() {
    }

    public static AddQuestionCommand getInstance() {
        return instance;
    }

    @Override
    public String execute(HttpServletRequest request) {
        String page = Constants.SET_ANSWER;
        HttpSession session = request.getSession();
        String quizName = session.getAttribute("quizName").toString();
        String[] questions = request.getParameterValues("question");
        List<String> answersText = Arrays.asList(request.getParameterValues("answer"));
        try {
            int quizId = AddQuestionService.getQuizId(quizName);
            List<Integer> questionsId = AddQuestionService.getQuestionsId(quizId, questions);
            AddQuestionService.saveAnswers(answersText, questionsId);
            request.setAttribute("questions", QuizService.getQuestions(quizId));
            session.setAttribute("quizId", quizId);
        } catch (ServiceException e) {
            LOG.error(e);
            page = Constants.ERROR;
        }
        return page;
    }
}