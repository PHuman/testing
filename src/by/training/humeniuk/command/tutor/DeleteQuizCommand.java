package by.training.humeniuk.command.tutor;

import by.training.humeniuk.command.Command;
import by.training.humeniuk.service.authorization.LoginService;
import by.training.humeniuk.service.exception.ServiceException;
import by.training.humeniuk.service.tutor.DeleteQuizService;
import by.training.humeniuk.util.Constants;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

public class DeleteQuizCommand implements Command {
    private static DeleteQuizCommand instance = new DeleteQuizCommand();
    private static final Logger LOG = Logger.getLogger(DeleteQuizCommand.class);

    private DeleteQuizCommand() {
    }

    public static DeleteQuizCommand getInstance() {
        return instance;
    }

    @Override
    public String execute(HttpServletRequest request) {
        String page = Constants.ERROR;
        int quizId = Integer.parseInt(request.getParameter("quiz"));
        try {
            DeleteQuizService.deleteQuiz(quizId);
            request.setAttribute("success_delete", true);
            request.setAttribute("subjects", LoginService.getSubjects());
            page = Constants.TUTOR_MENU;
        } catch (ServiceException e) {
            LOG.error(e);
        }
        return page;
    }


}
