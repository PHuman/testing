package by.training.humeniuk.command.tutor;

import by.training.humeniuk.command.Command;
import by.training.humeniuk.service.exception.ServiceException;
import by.training.humeniuk.service.tutor.AddQuizService;
import by.training.humeniuk.util.Constants;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class AddQuizCommand implements Command {
    private static AddQuizCommand instance = new AddQuizCommand();
    private static final Logger LOG = Logger.getLogger(AddQuizCommand.class);

    private AddQuizCommand() {
    }

    public static AddQuizCommand getInstance() {
        return instance;
    }

    @Override
    public String execute(HttpServletRequest request) {
        String page = Constants.ADD_QUESTION;
        HttpSession session = request.getSession();
        int subjectId = Integer.parseInt(session.getAttribute("subjectId").toString());
        int count = Integer.parseInt(request.getParameter("count"));
        request.setAttribute("count", count);
        String quizName = request.getParameter("quiz");
        session.setAttribute("quizName", quizName);
        try {
            AddQuizService.saveQuiz(subjectId, quizName);
        } catch (ServiceException e) {
            page = Constants.ERROR;
        }
        return page;
    }
}
