package by.training.humeniuk.command;

import by.training.humeniuk.command.authorization.LoginCommand;
import by.training.humeniuk.command.authorization.RegistrationCommand;
import by.training.humeniuk.command.common.LanguageCommand;
import by.training.humeniuk.command.common.LogoutCommand;
import by.training.humeniuk.command.student.*;
import by.training.humeniuk.command.tutor.*;

public enum CommandType {

    LOGIN {
        {
            this.command = LoginCommand.getInstance();
        }
    },

    REGISTRATION {
        {
            this.command = RegistrationCommand.getInstance();
        }
    },
    SUBJECT {
        {
            this.command = SubjectCommand.getInstance();
        }
    },
    QUIZ {
        {
            this.command = QuizCommand.getInstance();
        }
    },
    QUESTION {
        {
            this.command = QuestionCommand.getInstance();
        }
    },
    ADD_QUIZ {
        {
            this.command = AddQuizCommand.getInstance();
        }
    },
    ADD_QUESTION {
        {
            this.command = AddQuestionCommand.getInstance();
        }
    },
    SET_ANSWER {
        {
            this.command = SetAnswerCommand.getInstance();
        }
    },
    LANGUAGE {
        {
            this.command = LanguageCommand.getInstance();
        }
    },
    LOGOUT {
        {
            this.command = LogoutCommand.getInstance();
        }
    },
    STUDENT_MENU {
        {
            this.command = StudentMenuCommand.getInstance();
        }
    },
    TUTOR_MENU {
        {
            this.command = TutorMenuCommand.getInstance();
        }
    },
    DELETE_QUIZ {
        {
            this.command = DeleteQuizCommand.getInstance();
        }
    },
    RESULT {
        {
            this.command = ResultCommand.getInstance();
        }
    };
    Command command;

    public Command getCommand() {
        return command;
    }
}