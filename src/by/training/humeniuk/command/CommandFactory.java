package by.training.humeniuk.command;

import by.training.humeniuk.command.common.NullCommand;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

public class CommandFactory {
    private static CommandFactory instance = new CommandFactory();

    private CommandFactory() {
    }

    public static CommandFactory getInstance() {
        return instance;
    }

    public Command getCommand(HttpServletRequest request) {
        String action = request.getParameter("command");
        Command command;
        if (action == null || action.isEmpty()) {
            return NullCommand.getInstance();
        }
        CommandType commandType = CommandType.valueOf(action.toUpperCase());
        command = commandType.getCommand();
        return command;
    }

}
