<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <fmt:setLocale value="${locale}" scope="session"/>
    <fmt:setBundle basename="resources.text"/>
    <link rel="stylesheet" href="/css/style.css">
    <title><fmt:message key="Registration.title"/></title>
</head>
<body>
<form action="/controller" method="POST" class="form" accept-charset="UTF-8">
    <input type="hidden" name="command" value="registration">

    <h1 class="form__title"><fmt:message key="Registration.title"/></h1>
    <input type="text" placeholder="<fmt:message key="Registration.login"/>" pattern="[а-яА-ЯёЁa-zA-Z]{4,10}" required="required"
           class="form__input" name="login"/>
    <input type="text" placeholder="<fmt:message key="Registration.first_name"/>" pattern="[а-яА-ЯёЁa-zA-Z]{1,20}" required="required"
           class="form__input" name="first_name"/>
    <input type="text" placeholder="<fmt:message key="Registration.last_name"/>" pattern="[а-яА-ЯёЁa-zA-Z]{1,20}" required="required"
           class="form__input" name="last_name"/>
    <input type="password" placeholder="<fmt:message key="Registration.password"/>" pattern=".{5,20}"
           required="required" class="form__input" name="password"/>
    <input type="password" placeholder="<fmt:message key="Registration.confirmPassword"/>" pattern=".{5,20}"
           required="required" class="form__input" name="conf_password"/>
    <input type="password" placeholder="<fmt:message key="Registration.secret"/>" pattern="tutor"
           class="form__input" name="secret"/>
    <input type="submit" value="<fmt:message key="Registration.register"/>" class="form__submit"/>

    <div class="error">
        <c:if test="${secretError == true}"><fmt:message key="Registration.secretError"/></c:if>
        <c:if test="${emptyFields == true}"><fmt:message key="Registration.emptyFields"/></c:if>
        <c:if test="${passwordError == true}"><fmt:message key="Registration.passwordError"/></c:if>
        <c:if test="${errorRegistration == true}"><fmt:message key="Registration.errorRegistration"/></c:if>
    </div>
</form>

</body>
</html>
