<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <fmt:setLocale value="${locale}" scope="session"/>
    <fmt:setBundle basename="resources.text"/>
    <link rel="stylesheet" href="/css/style.css">
    <title><fmt:message key="AddQuiz.title"/></title>
</head>
<body>
<div>
    <c:import url="/jsp/logout.jsp" charEncoding="utf-8"/>
</div>
<form action="/controller" method="POST" class="form" accept-charset="UTF-8">
    <input type="hidden" name="command" value="add_quiz">

    <h1 class="form__title"><fmt:message key="AddQuiz.change"/></h1>
    <input type="text" placeholder="<fmt:message key="AddQuiz.quizName"/>" pattern=".{4,10}" required="required"
           class="form__input" name="quiz"/>
    <input type="text" placeholder="<fmt:message key="AddQuiz.count"/>" pattern="[1-9]{1}|(10)" required="required"
           class="form__input" name="count"/>
    <input type="submit" value="<fmt:message key="Subject.submit"/>" class="form__submit"/>
</form>
</body>
</html>
