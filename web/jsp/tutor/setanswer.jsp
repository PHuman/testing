<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <fmt:setLocale value="${locale}" scope="session"/>
    <fmt:setBundle basename="resources.text"/>
    <link rel="stylesheet" href="/css/style.css">
    <title><fmt:message key="Question.title"/></title>
</head>
<body>
<div>
    <c:import url="/jsp/logout.jsp" charEncoding="utf-8"/>
</div>
<form action="/controller" method="POST" class="form" accept-charset="UTF-8">
    <input type="hidden" name="command" value="set_answer">

    <h1 class="form__title"><fmt:message key="Question.change"/></h1>
    <ol>
        <c:forEach var="question" items="${questions}">
            <input type="hidden" name="question" value="${question.id}">
            <li>
                    ${question.questionText}
            </li>
            <ul>
                <c:forEach var="answer" items="${question.answers}">
                    <li>
                        <p><label>
                            <input name="answer" type="checkbox" value="${answer.id}">
                        </label> ${answer.answerText}</p>
                    </li>
                </c:forEach>
            </ul>
        </c:forEach>
    </ol>
    <p><input type="submit" value="<fmt:message key="Question.submit"/>"></p>
    <div class="error">
        <c:if test="${error == true}"><fmt:message key="Question.error"/></c:if>
    </div>
</form>
</body>
</html>
