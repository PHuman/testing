<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <fmt:setLocale value="${locale}" scope="session"/>
    <fmt:setBundle basename="resources.text"/>
    <link rel="stylesheet" href="/css/style.css">
    <title><fmt:message key="DeleteQuiz.title"/></title>
</head>
<body>
<div>
    <c:import url="/jsp/logout.jsp" charEncoding="utf-8"/>
</div>
<form action="/controller" method="POST" class="form" accept-charset="UTF-8">
    <input type="hidden" name="command" value="delete_quiz">

    <h1 class="form__title"><fmt:message key="Quiz.change"/></h1>
    <c:forEach var="quiz" items="${quizs}">
        <p><label>
            <input name="quiz" type="radio" value="${quiz.id}" required="required">
        </label> ${quiz.quizName}</p>
    </c:forEach>
    <p><input type="submit" value="<fmt:message key="DeleteQuiz.title"/>"></p>
</form>
</body>
</html>
