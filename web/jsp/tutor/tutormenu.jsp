<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <fmt:setLocale value="${locale}" scope="session"/>
    <fmt:setBundle basename="resources.text"/>
    <link rel="stylesheet" href="/css/style.css">
    <title><fmt:message key="TutorMenu.title"/></title>
</head>
<body>
<div>
    <c:import url="/jsp/logout.jsp" charEncoding="utf-8"/>
</div>
<form action="/controller" method="POST" class="form">
    <h1 class="form__title"><fmt:message key="TutorMenu.menu"/></h1>
    <input type="hidden" name="command" value="tutor_menu"/>
    <c:forEach var="subject" items="${subjects}">
        <p><label>
            <input name="subject" type="radio" value="${subject.id}" required="required">
        </label> ${subject.subjectName}</p>
    </c:forEach>
    <button type="submit" name="choice" value="result"><fmt:message key="TutorMenu.result"/></button>
    <button type="submit" name="choice" value="delete"><fmt:message key="TutorMenu.delete"/></button>
    <button type="submit" name="choice" value="add"><fmt:message key="TutorMenu.add"/></button
    <div class="error">
        <c:if test="${error == true}"><fmt:message key="TutorMenu.message"/></c:if>
        <c:if test="${success_delete == true}"><fmt:message key="TutorMenu.success.delete"/></c:if>
        <c:if test="${success == true}"><fmt:message key="AddQuiz.success"/></c:if>

    </div>
</form>
</body>
</html>
