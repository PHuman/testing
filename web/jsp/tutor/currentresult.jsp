<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <fmt:setLocale value="${locale}" scope="session"/>
    <fmt:setBundle basename="resources.text"/>
    <link rel="stylesheet" href="/css/style.css">
    <title><fmt:message key="Result.title"/></title>
</head>
<body>
<div>
    <c:import url="/jsp/logout.jsp" charEncoding="utf-8"/>
</div>
<form class="form">
    <table cellspacing="0" cellpadding="5" border="1" align="center" width="200">
        <c:forEach var="result" items="${result}">
            <tr>
                <td><c:out value="${result.login}"/></td>
                <td><c:out value="${result.quiz}"/></td>
                <td><c:out value="${result.result}"/></td>
            </tr>
        </c:forEach>
    </table>
</form>
</body>
</html>
