<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="/WEB-INF/forTag" %>
<html>
<head>
    <fmt:setLocale value="${locale}" scope="session"/>
    <fmt:setBundle basename="resources.text"/>
    <link rel="stylesheet" href="/css/style.css">
    <title><fmt:message key="AddQuestion.title"/></title>
</head>
<body>
<div>
    <c:import url="/jsp/logout.jsp" charEncoding="utf-8"/>
</div>
<form action="/controller" method="POST" class="form" accept-charset="UTF-8">
    <input type="hidden" name="command" value="add_question">

    <h1 class="form__title"><fmt:message key="AddQuestion.form"/></h1>
    <ol>
        <f:for to="${count}">
            <li></li>
            <input type="text" placeholder="<fmt:message key="AddQuestion.question"/>" required="required"
                   class="form__input" name="question"/>

            <input type="text" placeholder="<fmt:message key="AddQuestion.answer"/>" required="required"
                   class="form__input" name="answer"/>
            <input type="text" placeholder="<fmt:message key="AddQuestion.answer"/>" required="required"
                   class="form__input" name="answer"/>
            <input type="text" placeholder="<fmt:message key="AddQuestion.answer"/>" class="form__input"
                   name="answer"/>
            <input type="text" placeholder="<fmt:message key="AddQuestion.answer"/>" class="form__input"
                   name="answer"/>
        </f:for>
    </ol>
    <input type="submit" value="<fmt:message key="Question.submit"/>" class="form__submit"/>
</form>
</body>
</html>
