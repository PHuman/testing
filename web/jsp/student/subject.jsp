<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <fmt:setLocale value="${locale}" scope="session"/>
    <fmt:setBundle basename="resources.text"/>
    <link rel="stylesheet" href="/css/style.css">
    <title><fmt:message key="Subject.title"/></title>
</head>
<body>
<div>
    <c:import url="/jsp/logout.jsp" charEncoding="utf-8"/>
</div>
<form action="/controller" method="POST" class="form" accept-charset="UTF-8">
    <input type="hidden" name="command" value="subject">

    <h1 class="form__title"><fmt:message key="Subject.change"/></h1>
    <c:forEach var="subject" items="${subjects}">
        <p><label>
            <input name="subject" type="radio" value="${subject.id}" required="required">
        </label> ${subject.subjectName}</p>
    </c:forEach>
    <p><input type="submit" value="<fmt:message key="Subject.submit"/>"></p>

    <div class="error">
        <c:if test="${error == true}"><fmt:message key="Subject.error"/></c:if>
    </div>
</form>
</body>
</html>
