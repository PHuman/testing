<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <fmt:setLocale value="${locale}" scope="session"/>
    <fmt:setBundle basename="resources.text"/>
    <link rel="stylesheet" href="/css/style.css">
    <title><fmt:message key="Result.title"/></title>
</head>
<body>
<div>
    <c:import url="/jsp/logout.jsp" charEncoding="utf-8"/>
</div>
<form action="/controller" method="POST" class="form" accept-charset="UTF-8">
    <input type="hidden" name="command" value="result">

    <h1 class="form__title"><fmt:message key="Result.message"/> ${result}%</h1>
    <button type="submit" name="choice" value="subject"><fmt:message key="Result.button"/></button>
</form>
</body>
</html>
