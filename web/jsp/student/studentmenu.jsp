<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <fmt:setLocale value="${locale}" scope="session"/>
    <fmt:setBundle basename="resources.text"/>
    <link rel="stylesheet" href="/css/style.css">
    <title><fmt:message key="StudentMenu.title"/></title>
</head>
<body>
<div>
    <c:import url="/jsp/logout.jsp" charEncoding="utf-8"/>
</div>
<form action="/controller" method="POST" class="form">
    <h1 class="form__title"><fmt:message key="StudentMenu.title"/></h1>
    <input type="hidden" name="command" value="student_menu"/>
    <button type="submit" name="choice" value="result"><fmt:message key="StudentMenu.result"/></button>
    <button type="submit" name="choice" value="subject"><fmt:message key="StudentMenu.subject"/></button>
    <div class="error">
        <c:if test="${error == true}"><fmt:message key="StudentMenu.message"/></c:if>
    </div>
</form>
</body>
</html>
