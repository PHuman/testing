<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <fmt:setLocale value="${locale}" scope="session"/>
    <fmt:setBundle basename="resources.text"/>
    <link rel="stylesheet" href="/css/style.css">
    <title>Header</title>
</head>
<body>
<form action="/controller" method="POST" class="form">
    <input type="hidden" name="command" value="logout"/>
    <input name="language" type="submit" value="<fmt:message key="Logout.button"/>"/>
</form>
</body>
</html>
