<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <fmt:setLocale value="${locale}" scope="session"/>
    <fmt:setBundle basename="resources.text"/>
    <link rel="stylesheet" href="/css/style.css">
    <title><fmt:message key="Login.title"/></title>
</head>
<body>
<form action="/controller" method="POST" class="form">
    <input type="hidden" name="command" value="language"/>
    <input name="language" type="submit" value="EN"/>
    <input name="language" type="submit" value="RU"/>
</form>
<form action="/controller" method="POST" class="form">

    <input type="hidden" name="command" value="login">

    <h1 class="form__title"><fmt:message key="Login.title"/></h1>
    <input type="text" placeholder="<fmt:message key="Login.login"/>" pattern="[а-яА-ЯёЁa-zA-Z]{4,10}" required="required"
           class="form__input" name="login"/>
    <input type="password" placeholder="<fmt:message key="Login.password"/>" pattern=".{5,20}" required="required"
           class="form__input" name="password"/>
    <input type="submit" value="<fmt:message key="Login.enter"/>" class="form__submit"/>
    <div class="error">
        <c:if test="${successRegistration == true}"><fmt:message key="Registration.successRegistration"/></c:if>
        <c:if test="${errorLoginPassMessage == true}"><fmt:message key="Registration.errorLoginPassMessage"/></c:if>
    </div>
</form>
<form action="/controller" method="POST" class="form">
    <input type="hidden" name="command" value="login">
    <button type="submit" name="choice" value="registration"><fmt:message key="Registration.title"/></button
</form>
</body>
</html>
