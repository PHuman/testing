<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <fmt:setLocale value="${locale}" scope="session"/>
    <fmt:setBundle basename="resources.text"/>
    <link rel="stylesheet" href="/css/style.css">
    <title>Error</title>
</head>
<body>
<form class="form" accept-charset="UTF-8">
    <h1 class="form__title"><fmt:message key="Error.message"/></h1>
</form>
</body>
</html>
